package br.edu.unisep.gateway.security.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JwtUtils {

    private static final String TOKEN_KEY = "!#@!#S1st3m45_2020_##&%";
    private static final String BEARER_PREFIX = "Bearer ";

    public boolean isValid(String token) {
        try {
            var tempToken = token.replace(BEARER_PREFIX, "");
            Jwts.parser()
                    .setSigningKey(TOKEN_KEY)
                    .parseClaimsJws(tempToken);
        } catch (Exception exception) {
            return false;
        }

        return true;
    }

    public Claims getClaims(String token) {
        var tempToken = token.replace(BEARER_PREFIX, "");
        return Jwts.parser()
                .setSigningKey(TOKEN_KEY)
                .parseClaimsJws(tempToken)
                .getBody();
    }
}
